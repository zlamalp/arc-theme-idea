# Arc look-alike theme for Intellij IDEA 2019.3+

This is a light / dark version of Arc theme with default accented blue color (orange as optional, but my favourite).

* All credits to the original GTK theme authors:
  * https://github.com/horst3180/Arc-theme
  * https://github.com/NicoHood/arc-theme
  
### 2020.1 (Light)
  
![Semantic description of image](/img/2020.1/window.png)*IDE*
  
![Semantic description of image](/img/2020.1/window_plugins.png)*Plugins*
  
![Semantic description of image](/img/2020.1/window_search.png)*Search everywhere*
  
![Semantic description of image](/img/2020.1/window_settings.png)*Settings*
  
![Semantic description of image](/img/2020.1/window_vcs.png)*VCS*

### 2020.1 (Dark)
  
![Semantic description of image](/img/2020.1/window_dark.png)*IDE*
  
![Semantic description of image](/img/2020.1/window_plugins_dark.png)*Plugins*
  
![Semantic description of image](/img/2020.1/window_search_dark.png)*Search everywhere*
  
![Semantic description of image](/img/2020.1/window_settings_dark.png)*Settings*
  
![Semantic description of image](/img/2020.1/window_vcs_dark.png)*VCS*

### 2019.3

![Semantic description of image](/img/2019.3/window.png)*IDE*

![Semantic description of image](/img/2019.3/window_plugins.png)*Plugins*

![Semantic description of image](/img/2019.3/window_search.png)*Search everywhere*

![Semantic description of image](/img/2019.3/window_settings.png)*Settings*

![Semantic description of image](/img/2019.3/window_vcs.png)*VCS*
